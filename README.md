# CaesarCipher

The Caesar Cipher, also known as a shift cipher, is one of the oldest and simplest forms of encrypting a message. It is a type of substitution cipher where each letter in the original message is replaced with a letter corresponding to a certain number of letters shifted up or down in the alphabet. The method is named after Julius Caesar, who apparently used it to communicate with his generals. More about Ceaser Cipher --> https://privacycanada.net/classical-encryption/caesar-cipher/

**Implementation of Caesar Cipher Algorithm**

Caesar Cipher Algorithm contains two methods: 
*  encrypt - this method encrypts text according to Ceaser Cipher Algorithm
*  decode - this method deciphers encrypted text