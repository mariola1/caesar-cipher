import string


class CaesarCipher:
    def __init__(self):
        self.letters = string.ascii_lowercase
        self.alphabet_length = ord('Z')

    def encrypt(self, key, text_to_encrypt):
        upper_text_to_encrypt = text_to_encrypt.upper()
        encrypted_text = []
        position_cipher_text_letter = 0
        for character in upper_text_to_encrypt:
            if not character.isalpha():
                encrypted_text.append(character)
            else:
                for element in self.letters:
                    sum = ord(element) + key
                    if element == character and sum <= self.alphabet_length:
                        position_cipher_text_letter = sum
                        break
                    if element == character and sum > self.alphabet_length:
                        position_cipher_text_letter = sum - self.alphabet_length - 1 + ord('A')
                encrypted_text.append(chr(position_cipher_text_letter))
        s = ''.join(encrypted_text)
        return s

    def decode(self, key, text_to_decode):
        upper_text_to_decode = text_to_decode.upper()
        encrypted_text = []
        for character in upper_text_to_decode:
            if not character.isalpha():
                encrypted_text.append(character)
            else:
                for index, keys in enumerate(self.letters):
                    if keys == character:
                        difference = index - key
                        decrypted_letter = self.letters[difference]
                        encrypted_text.append(decrypted_letter)
                        break
        s = ''.join(encrypted_text)
        return s
