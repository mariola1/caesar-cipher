from unittest import TestCase

from src.caesar_cipher_ascii import CaesarCipher


class CaesarCipherTest(TestCase):
    def setUp(self):
        self.caesar = CaesarCipher()

    def test_encrypt(self):
        """
        Test that encrypt function return valid encrypted value
        """
        self.assertEqual(self.caesar.encrypt(7, 'TEST'), 'ALZA')

    def test_decode(self):
        """
        Test that decode function return valid decoded value
        """
        self.assertEqual(self.caesar.decode(7, 'ALZA'), 'TEST')
